package SpaceBotWar::DB::Result;

no warnings qw(uninitialized);
use namespace::autoclean -except => ['meta'];

use base 'DBIx::Class::Core';

__PACKAGE__->load_components('TimeStamp', 'InflateColumn::DateTime', 'InflateColumn::Serializer', 'Core');

1;

