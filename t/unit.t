use strict;
use warnings;
use Test::Most;
use Log::Log4perl;
use lib "lib";
use lib "t/lib";

use SpaceBotWar::DB;
use SpaceBotWar::SDB;

use Test::Class::Moose::Load 't/lib/UnitTestsFor';
use Test::Class::Moose::Runner;

Log::Log4perl::init('etc/log4perl.conf');

#--- Initialize the database singleton
#
my $db = SpaceBotWar::DB->connect(
    'DBI:SQLite:/Users/icydee/docker/core/log/test.db',
);
$db->deploy({ add_drop_table => 1 });

SpaceBotWar::SDB->initialize({
    db      => $db,
});

my $runner = Test::Class::Moose::Runner->new(test_classes => \@ARGV );
$runner->runtests;

1;
