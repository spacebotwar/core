package DBFixtures;

use Moose;
extends 'DBIx::Class::EasyFixture';

my $definitions_for = {
    user_albert => {
        new => 'User',
        using => {
            id          => 1,
            username    => 'bert',
            password    => '{SSHA}KnIrp466EYjf16NptDR9bnhjCI5z6D14', # this is encrypted 'secret'
            email       => 'bert@example.com',
        },
    },
    one_user    => {
        new     => 'User',
        using   => {
            username    => 'test_user_1',
            password    => 'encrypted',
            email       => 'me@example.com',
        },
    },
};

sub get_definition {
    my ($self, $name) = @_;

    return $definitions_for->{$name};
}

sub all_fixture_names {
    return keys %$definitions_for;
}

__PACKAGE__->meta->make_immutable;

