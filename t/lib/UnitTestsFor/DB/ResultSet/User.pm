package UnitTestsFor::DB::ResultSet::User;

use lib 'lib';
use lib 't/lib';

use Test::Class::Moose;

use SpaceBotWar::SDB;
use DBFixtures;

sub test_create_valid_user {
    my ($self) = @_;

    my $db = SpaceBotWar::SDB->instance;

    my $open_password = 'Yop_s3cr3t';
    my $user = $db->resultset('User')->assert_create({
        username    => ' test_user_1',
        password    => $open_password,
        email       => 'me@example.com',
    });

    ok($user, 'created new user');
    isnt($user->password, $open_password, 'Password encrypted');
}

sub test_cant_create_duplicate_user {
    my ($self) = @_;

    my $db = SpaceBotWar::SDB->instance->db;
    my $fixtures = DBFixtures->new({ schema => $db });
    $fixtures->load('one_user');

    my $user;

    eval {
        $user = $db->resultset('User')->assert_create({
            username    => 'test_user_1',
            password    => 'Yop_s3cr3t',
            email       => 'me@example.com',
        });
    };
    is($user, undef, 'cant create a duplicate username');

    $fixtures->unload;
}
__PACKAGE__->meta->make_immutable;

